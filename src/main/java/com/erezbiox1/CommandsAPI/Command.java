//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.erezbiox1.CommandsAPI;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Command {
    String name() default "";

    String permission() default "";

    String arguments() default "";

    String permissionError() default "default";

    String argumentsError() default "default";

    String playerError() default "default";
}
