package com.dizmizzer.uhc.Commands.Console;

import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.Util.DizUtils;
import com.dizmizzer.uhc.Util.PunishType;
import com.erezbiox1.CommandsAPI.Command;
import com.erezbiox1.CommandsAPI.CommandListener;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class Kick extends DizUtils implements CommandListener {

    public Kick() {
    }

    @Command()
    public void kick(CommandSender p, String[] args) {
        if (!p.hasPermission("uhc.kick")) {
            sendMessage(p, StringManager.get().getString("noperm"), false);
            return;
        }

        if (args.length < 2) {
            sendMessage(p, StringManager.get().getString("notenoughargs"), false);
            return;
        }

        Player player = Bukkit.getPlayer(args[0]);

        if (player == null) {
            sendMessage(p, StringManager.get().getString("isoffline"), false);
            return;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            sb.append(args[i] + " ");
        }

        String kick_reason = sb.toString();

        player.kickPlayer(StringManager.get().kickPlayer(PunishType.KICK, kick_reason, player, p));

        

     }

}
