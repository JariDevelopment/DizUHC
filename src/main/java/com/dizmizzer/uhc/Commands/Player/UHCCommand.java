package com.dizmizzer.uhc.Commands.Player;

import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.DizUtils;
import com.dizmizzer.uhc.Util.GameState;
import com.erezbiox1.CommandsAPI.Command;
import com.erezbiox1.CommandsAPI.CommandListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class UHCCommand extends DizUtils implements CommandListener {
    UHC u;
    Plugin p;

    public UHCCommand(UHC uhc, Plugin plugin) {
        this.u = uhc;
        this.p = plugin;
    }

    @Command
    public void uhc(Player player, String[] args) {
        if (!player.hasPermission("uhc.host")) {
            sendMessage(player, StringManager.get().getString("noperm"), true);
            return;
        }

        if (u.gameState != GameState.PREGAME) {
            sendMessage(player, StringManager.get().getString("nopregame"), true);
            return;
        }
        u.gameState = GameState.INGAME;


    }

    public void startGame() {
        Bukkit.getWorld("UHC").getWorldBorder().setCenter(0, 0);
        Bukkit.getWorld("UHC").getWorldBorder().setSize(1500);

        BukkitRunnable br = new BukkitRunnable() {
            int start = 0;

            @Override
            public void run() {
                if (start == 3600) {

                } else {
                    Bukkit.getWorld("UHC").getWorldBorder().setSize(1500 - (1000 / 3600) * start);

                    start++;
                }
            }
        };
        br.runTaskTimer(p, 20, 20);
    }
}
