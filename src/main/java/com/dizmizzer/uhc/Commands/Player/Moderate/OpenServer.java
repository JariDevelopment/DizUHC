package com.dizmizzer.uhc.Commands.Player.Moderate;

import com.dizmizzer.uhc.Managers.GameManager;
import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.GameState;
import com.erezbiox1.CommandsAPI.Command;

import com.erezbiox1.CommandsAPI.CommandListener;
import org.bukkit.entity.Player;

import javax.naming.NoPermissionException;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */

public class OpenServer implements CommandListener {

    private final UHC uhc;

    public OpenServer(UHC u) { this.uhc = u; }
    @Command
    public void open(Player player, String args[]) {

        if (!GameManager.getInstance().getHost().equalsIgnoreCase(player.getName().toLowerCase())) {
            player.sendMessage(StringManager.get().getString("noperm"));
            return;
        }

        if (uhc.gameState != GameState.SETUP) {
            player.sendMessage("alreadyOpen");
            return;
        }

        uhc.gameState = GameState.PREGAME;



    }
}
