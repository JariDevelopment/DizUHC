package com.dizmizzer.uhc.Util;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public enum GameState {
    SETUP(),
    PREGAME(),
    INGAME(),
    POSTGAME()
}
