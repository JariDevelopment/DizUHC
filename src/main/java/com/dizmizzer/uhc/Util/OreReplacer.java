package com.dizmizzer.uhc.Util;

import com.dizmizzer.uhc.UHC;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.generator.BlockPopulator;

import java.util.Random;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class OreReplacer extends BlockPopulator {
    UHC uhc;
    Block block;

    public OreReplacer(UHC u) {
        this.uhc = u;
    }

    @Override
    public void populate(World world, Random random, Chunk chunk) {
        for (int x = 0; x < 16; x++)
            for (int y = 1; y < 128; y++)
                for (int z = 0; z < 16; z++) {
                    block = chunk.getBlock(x, y, z);
                    if (block.getType() == Material.GRASS) block.setType(Material.EMERALD_ORE);
                    if (uhc.getScen().containsKey(Scenario.GOLDLESS)) {
                        if (uhc.getScen().get(Scenario.GOLDLESS)) {
                            if (block.getType() == Material.GOLD_ORE) {
                                block.setType(Material.STONE);
                            }
                        }
                    }
                    if (uhc.getScen().containsKey(Scenario.DIAMONDLESS)) {
                        if (uhc.getScen().containsKey(Scenario.DIAMONDLESS)) {
                            if (block.getType() == Material.DIAMOND_ORE) {
                                block.setType(Material.STONE);
                            }
                        }
                    }
                }
    }
}
