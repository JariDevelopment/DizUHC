package com.dizmizzer.uhc.Managers;

import com.dizmizzer.uhc.Util.GameState;
import com.dizmizzer.uhc.Util.Scenario;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class GameManager {

    static GameManager instance = new GameManager();
    File file;
    FileConfiguration config;

    public static GameManager getInstance() {
        return instance;
    }

    public void setup(Plugin p) {

        if (!p.getDataFolder().exists()) p.getDataFolder().mkdir();

        file = new File(p.getDataFolder(), "hosts.yml");
        if (!file.exists()) {
            try {
                FileUtils.copyURLToFile(new URL("https://pastebin.com/raw/FPEwQciA"), file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        config = YamlConfiguration.loadConfiguration(file);
    }

    public String getHost() {
        return config.getString("host");
    }

    public String[] getMods() {
        return config.getString("mods").replace("[", "").replace("]", "").replace(" ", "").toLowerCase().split(",");
    }

    public String getMOTD(GameState gameState) {
        return config.getString("motd." + gameState.toString().toLowerCase());
    }

    public ArrayList<Scenario> getScenarios() {
        String[] scenarios = config.getString("scenarios").replace("[", "").replace("]", "").toUpperCase().replace(" ", "").split(",");
        ArrayList<Scenario> arr = new ArrayList<>();
        for (String s : scenarios) {
            try {
                if (Scenario.valueOf(s) != null) arr.add(Scenario.valueOf(s));
            } catch (IllegalArgumentException e) {
                Bukkit.getLogger().severe(StringManager.get().getString("ScenarioNull", s));
            }
        }
        return arr;
    }

}
