package com.dizmizzer.uhc.Managers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */

public class PunishManager {

    static PunishManager instance = new PunishManager();
    File file;
    FileConfiguration con;

    public static PunishManager getInstance() {
        return instance;
    }

    public synchronized boolean isMuted(UUID id) {
        return false;
    }

    public synchronized void addMute(Player player, int date, String time, String reason) {

    }

    public synchronized void addBan(Player player, int date, String time, String reason) {

    }

    public void setup(Plugin p) {
        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdirs();
        }

        file = new File(p.getDataFolder(), "MuteSQL.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
                con = YamlConfiguration.loadConfiguration(file);
                con.set("host", "localhost");
                con.set("username", "root");
                con.set("port", 3306);
                con.set("password", "password");
                con.set("database", "minecraftdb");
                try {
                    con.save(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        con = YamlConfiguration.loadConfiguration(file);

    }

    public synchronized void connect() {

    }


}
