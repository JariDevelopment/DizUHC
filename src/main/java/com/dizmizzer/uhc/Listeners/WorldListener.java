package com.dizmizzer.uhc.Listeners;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.OreReplacer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldInitEvent;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class WorldListener implements Listener {
    UHC u;

    public WorldListener(UHC uhc) {
        this.u = uhc;
    }

    @EventHandler
    public void onLoad(WorldInitEvent e) {
        e.getWorld().getPopulators().add(new OreReplacer(u));
    }
}

