package com.dizmizzer.uhc.Listeners;

import com.dizmizzer.uhc.Managers.PunishManager;
import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.Util.DizUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class ChatListener extends DizUtils implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (PunishManager.getInstance().isMuted(e.getPlayer().getUniqueId())) {
            sendMessage(e.getPlayer(), StringManager.get().getString("ismuted"), false);
            e.setCancelled(true);
            return;
        }
    }
}
