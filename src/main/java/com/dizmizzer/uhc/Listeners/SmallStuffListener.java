package com.dizmizzer.uhc.Listeners;

import com.dizmizzer.uhc.Managers.SQLManager;
import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.DizUtils;
import com.dizmizzer.uhc.Util.GameState;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */

public class SmallStuffListener extends DizUtils implements Listener {
    UHC u;

    public SmallStuffListener(UHC uhc, Plugin p) {
        this.u = uhc;
        if (u.debug) {
            BukkitRunnable br = new BukkitRunnable() {
                @Override
                public void run() {
                    if (Bukkit.getWorlds().get(0).getTime() > 8000)
                        Bukkit.getWorlds().get(0).setTime(0);
                }
            };
             br.runTaskTimer(p, 40, 40);
        }

    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (u.gameState == GameState.SETUP) {
            if (!u.customWhitelist.contains(e.getPlayer().getName().toLowerCase())) {
                e.getPlayer().kickPlayer(StringManager.toColor("&cThe game is getting ready to set up!"));
            }
        }
        //SQLManager.getInstance().addPlayer(e.getPlayer());
    }
}
