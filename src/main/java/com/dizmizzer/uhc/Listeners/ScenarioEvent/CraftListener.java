package com.dizmizzer.uhc.Listeners.ScenarioEvent;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.Scenario;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class CraftListener implements Listener {

    UHC u;

    public CraftListener(UHC u) {
        this.u = u;
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        if (u.getScen().get(Scenario.BOWLESS)) {
            if (e.getRecipe().getResult().getType() == Material.BOW) {
                e.setCancelled(true);
            }
        }


    }

}
