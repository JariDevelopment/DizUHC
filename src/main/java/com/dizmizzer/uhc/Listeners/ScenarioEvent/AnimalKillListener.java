package com.dizmizzer.uhc.Listeners.ScenarioEvent;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.Scenario;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class AnimalKillListener implements Listener {
    UHC u;

    public AnimalKillListener(UHC uhc) {
        this.u = uhc;
    }

    @EventHandler
    public void onKill(EntityDeathEvent e) {
        if (e.getEntity().getKiller() == null) {
            return;
        }

        if (!(e.getEntity().getKiller() instanceof Player)) {
            return;
        }
        if (u.getScen().containsKey(Scenario.CUTCLEAN)) {
            if (u.getScen().get(Scenario.CUTCLEAN)) {

                for (ItemStack drop : e.getDrops()) {
                    if (drop.getType() == Material.RAW_BEEF) {
                        int amount = drop.getAmount();
                        e.getDrops().remove(drop);
                        e.getDrops().add(new ItemStack(Material.COOKED_BEEF, amount));
                    }
                    if (drop.getType() == Material.PORK) {
                        int amount = drop.getAmount();
                        e.getDrops().remove(drop);
                        e.getDrops().add(new ItemStack(Material.GRILLED_PORK, amount));

                    }
                    if (drop.getType() == Material.RAW_CHICKEN) {
                        int amount = drop.getAmount();
                        e.getDrops().remove(drop);
                        e.getDrops().add(new ItemStack(Material.COOKED_CHICKEN, amount));
                    }
                    if (drop.getType() == Material.MUTTON) {
                        int amount = drop.getAmount();
                        e.getDrops().remove(drop);
                        e.getDrops().add(new ItemStack(Material.COOKED_MUTTON, amount));
                    }
                    if (drop.getType() == Material.RABBIT) {
                        int amount = drop.getAmount();
                        e.getDrops().remove(drop);
                        e.getDrops().add(new ItemStack(Material.COOKED_RABBIT, amount));
                    }
                }
            }
        }

    }
}
