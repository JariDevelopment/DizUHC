package com.dizmizzer.uhc.Listeners.ScenarioEvent;

import com.dizmizzer.uhc.UHC;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class PlayerKillListener implements Listener {

    UHC u;

    public PlayerKillListener(UHC uhc) {
        this.u = uhc;
    }

    @EventHandler
    public void onKill(EntityDeathEvent e) {
        if (e.getEntity().getKiller() == null) return;
        if (e.getEntity().getKiller() instanceof Player && e.getEntity().getType() == EntityType.PLAYER) {

        }
    }

}
