package com.dizmizzer.uhc.Listeners;

import com.dizmizzer.uhc.Managers.StringManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class DeathListeners implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        switch (e.getEntity().getLastDamageCause().getCause()) {
            case FALL:
                Bukkit.broadcastMessage(StringManager.get().getString("ondeathfall"));
                break;
            default:

        }
    }
}
