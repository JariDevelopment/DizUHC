![alt text](http://i.imgur.com/2f6KPNF.png "Logo.png")


Hosting
-
  Files needed for download:
  - Messages [here]()
  - Host.yml [here]()
  

PlaceHolders:
-
  - %player% - Targeted Player.
  - %punisher% - User running the command.
  - %reason% - reason why player got punished.
  - %scenario% - Targeted scenario.

TODO:
-
- [ ] Scenarios
  - [x] 1/2 Ores - For every 2 ores you mine, 1 drops. Every single ore is seperate from one and another.
  - [x] Bowless - No bows
  - [x] Cutclean - No melting
  - [x] Goldless - No gold
  - [x] Diamondless - No Diamonds
  - [x] Triple Arrows - 3 arrows shot
  - [ ] AppleFamine - No apples
  - [ ] Assassins - Each player has a target that they must kill. Killing anyone that is not your target or assassin will result in no items dropping. When your target dies, you get their target.
  - [x] Names
  - [x] Descriptions
- [ ] Commands
  - [ ] kick
  - [ ] ban
  - [ ] mute
  - [x] toggle scenario
  - [x] scenario show
- [ ] Post-Death
  - [ ] Invisible handler
- [ ] Ingame
  - [ ] Killing
  - [ ] Points
  - [ ] Rewards
  - [ ] Dropping
  - [ ] Death
  - [ ] Winning
- [ ] Host stuff
  - [ ] Able to setup stuff
  - [ ] Make host temp owner